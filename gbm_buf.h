struct buffer {
	int busy;
	int drm_fd;
	int dmabuf_fd;

	int bpp;
	int cpp;
	int width;
	int height;
	int format;
	uint8_t *mmap;
	void *map_data;
	uint32_t gem_handle;
	uint32_t dst_stride;
	uint32_t stride;

	struct gbm_bo *bo;
	struct gbm_device *gbm;
};

struct buffer *create_dma_buffer(int width, int height, int format);
void destroy_dma_buffer(struct buffer *b);
int gbm_init(struct buffer *buf);
void gbm_fini(struct buffer *buf);