all:
	gcc -c vaapi_hw_decode.c -I/usr/include/drm -I/include -g
	gcc -c gbm_buf.c -I/usr/include/drm -I/include -g
	gcc -c fbdev_draw.c -I/usr/include/drm -I/include -g
	gcc -c drm_draw_pixels.c -I/usr/include/drm -I/include -g
	gcc -o vaapi_hw_decode vaapi_hw_decode.o gbm_buf.o fbdev_draw.o drm_draw_pixels.o -g -lavformat -lavcodec -lavutil -lgbm -ldrm -lva
	 

clean:
	rm -rf vaapi_hw_decode *.o
