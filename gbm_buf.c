#include <xf86drm.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <drm_fourcc.h>
#include <gbm.h>
#include <libavformat/avformat.h>
#include "gbm_buf.h"

static uint32_t av_format_to_drm_format(int format)
{
	switch (format) {
	case AV_PIX_FMT_YUV420P:
		return DRM_FORMAT_YUV420;
	case AV_PIX_FMT_YUV420P10BE:
	case AV_PIX_FMT_YUV420P10LE:
		return DRM_FORMAT_P010;
	case AV_PIX_FMT_YUV420P12BE:
	case AV_PIX_FMT_YUV420P12LE:
		return DRM_FORMAT_P012;
	case AV_PIX_FMT_YUV420P16BE:
	case AV_PIX_FMT_YUV420P16LE:
		return DRM_FORMAT_P016;
	default:
		return -1;
	}
}

void destroy_dma_buffer(struct buffer *b)
{
	gbm_bo_unmap(b->bo, b->map_data);
	gbm_fini(b);
}

struct buffer *create_dma_buffer(int width, int height, int av_format)
{
	struct buffer *buffer;
	uint64_t modifier = DRM_FORMAT_MOD_LINEAR;
	uint32_t flags = 0;
	unsigned buf_w, buf_h;
    int drm_format = av_format_to_drm_format(av_format);
	int pixel_format;

	buffer = malloc(sizeof(struct buffer));
	memset(buffer, 0, sizeof(struct buffer));

	if (gbm_init(buffer)) {
		fprintf(stderr, "drm_connect failed\n");
		goto error;
	}

	buffer->width = width;
	buffer->height = height;
	buffer->format = drm_format;

	switch (drm_format) {
	case DRM_FORMAT_NV12:
		pixel_format = GBM_FORMAT_GR88;
		buf_w = width / 2;
		buf_h = height * 3 / 2;
		buffer->cpp = 1;
		break;

	case DRM_FORMAT_YUV420:
		pixel_format = GBM_FORMAT_GR88;
		buf_w = width / 2;
		buf_h = height * 2;    /* U/V not interleaved */
		buffer->cpp = 1;
		break;

	case DRM_FORMAT_P010:
		pixel_format = GBM_FORMAT_GR88;
		buf_w = width;
		buf_h = height * 3 / 2;
		buffer->cpp = 2;
		break;

	case DRM_FORMAT_ARGB8888:
		pixel_format = GBM_FORMAT_ARGB8888;
		buf_w = width;
		buf_h = height;
		buffer->cpp = 1;
		break;

	default:
		pixel_format = GBM_FORMAT_XRGB8888;
		buf_w = width;
		buf_h = height;
		buffer->height = height;
		buffer->cpp = 1;
	}

	buffer->bo = gbm_bo_create_with_modifiers(buffer->gbm, buf_w, buf_h, pixel_format, &modifier, 1);
	if (!buffer->bo) {
		fprintf(stderr, "error: unable to allocate bo\n");
		goto error1;
	}

	buffer->dmabuf_fd = gbm_bo_get_fd(buffer->bo);
	buffer->stride = gbm_bo_get_stride(buffer->bo);

	if (buffer->dmabuf_fd < 0) {
		fprintf(stderr, "error: dmabuf_fd < 0\n");
		goto error2;
	}

	buffer->mmap = gbm_bo_map(buffer->bo, 0, 0, buffer->width, buffer->height,
				  GBM_BO_TRANSFER_WRITE, &buffer->dst_stride, &buffer->map_data);
	if (!buffer->mmap) {
		fprintf(stderr, "Unable to mmap buffer\n");
		goto error2;
	}

	return buffer;

error2:
	gbm_bo_destroy(buffer->bo);

error1:
	gbm_fini(buffer);

error:
	return NULL;
}

int gbm_init(struct buffer *buf)
{
	buf->drm_fd = open("/dev/dri/renderD128", O_RDWR);
	
	if (buf->drm_fd < 0)
		return -1;

	if (!buf->gbm)
		buf->gbm = gbm_create_device(buf->drm_fd);

	if (!buf->gbm)
		return -1;

	return 0;
}

void gbm_fini(struct buffer *b)
{
	gbm_device_destroy(b->gbm);
	close(b->drm_fd);
}