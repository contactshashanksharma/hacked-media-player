#include <unistd.h>
#include <sys/types.h>

int drm_display_buffer(uint8_t *buf, int size, int drm_fd);
int drm_init(void);