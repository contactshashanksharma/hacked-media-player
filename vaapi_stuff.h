typedef struct VSurfaceFormat {
    enum AVPixelFormat pix_fmt;
    VAImageFormat image_format;
} VSurfaceFormat;

typedef struct VDeviceContext {
    // Surface formats which can be used with this device.
    VSurfaceFormat *formats;
    int              nb_formats;
} VDeviceContext;

struct AVHWDeviceInternal {
    const void *hw_type;
    void                *priv;

    /**
     * For a derived device, a reference to the original device
     * context it was derived from.
     */
    AVBufferRef *source_device;
};

typedef struct AVHWDeviceInternal AVHWDeviceInternal;