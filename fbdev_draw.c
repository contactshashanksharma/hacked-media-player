#include <linux/fb.h>
#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "fbdev.h"
#define SIZE 1920 * 1080 * 4

void fbdev_close(struct fbdev *fb)
{
	munmap(fb->fbp, fb->screensize);
	close(fb->fb_fd);
	free(fb);
}

int fbdev_show(uint8_t *data, int size, struct fbdev *fb)
{
	if (size <= 0) {
		fprintf(stderr, "invalid size = %d, setting default\n", size);
		size = SIZE;
	}

	memcpy(fb->fbp, data, size < fb->screensize ? size : fb->screensize);
	return 0;
}

struct fbdev* fbdev_setup(void)
{
	struct fb_fix_screeninfo fixinfo;
	struct fb_var_screeninfo varinfo;
	long screensize;
	uint8_t *fbp;
	int fb_fd, ret;
	int x,y;
	struct fbdev *fb;

	fb_fd = open("/dev/fb0", O_RDWR);
	if (fb_fd < 0) {
		printf("Failed to open fbdev 0\n");
		return NULL;
	}

	/* Get variable screen info */
	ret = ioctl(fb_fd, FBIOGET_VSCREENINFO, &varinfo);
	if (ret < 0) {
		printf("Failed to get fbdev varinfo\n");
		close(fb_fd);
		return NULL;
	}

	varinfo.grayscale=0;
	varinfo.bits_per_pixel=32;

	/* Set variable screen info for new BPP */
	ret = ioctl(fb_fd, FBIOPUT_VSCREENINFO, &varinfo);
	if (ret < 0) {
		printf("Failed to set bpp\n");
		close(fb_fd);
		return NULL;
	}

	/* Get new variable info */
	ret = ioctl(fb_fd, FBIOGET_VSCREENINFO, &varinfo);
	if (ret < 0) {
		printf("Failed to get new variable info\n");
		close(fb_fd);
		return NULL;
	}

	ret = ioctl(fb_fd, FBIOGET_FSCREENINFO, &fixinfo);
	if (ret < 0) {
		printf("Failed to get fb fix info\n");
		close(fb_fd);
		return NULL;
	}

	screensize = varinfo.yres_virtual * fixinfo.line_length;
	if (!screensize) {
		printf("Zero screen size, pitch=%d y=%d\n", fixinfo.line_length, varinfo.yres_virtual);
		close(fb_fd);
		return NULL;
	}

	/* Map framebuffer memory in this app's space */
	fbp = mmap(0, screensize, PROT_READ | PROT_WRITE, MAP_SHARED, fb_fd, (off_t)0);
	if (!fbp) {
		printf("mmap failed\n");
		close(fb_fd);
		return NULL;
	}

	fb = malloc(sizeof(*fb));
	fb->fb_fd = fb_fd;
	fb->fbp = fbp;
	fb->screensize = screensize;
	return fb;
}
