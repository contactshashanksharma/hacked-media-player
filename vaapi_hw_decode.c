/*
 * Copyright (c) 2017 Jun Zhao
 * Copyright (c) 2017 Kaixuan Liu
 *
 * HW Acceleration API (video decoding) decode sample
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @file HW-accelerated decoding API usage.example
 * @example hw_decode.c
 *
 * Perform HW-accelerated decoding with output frames from HW video
 * surfaces.
 */

#include <stdio.h>

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/pixdesc.h>
#include <libavutil/pixfmt.h>
#include <libavutil/hwcontext.h>
#include <libavutil/hwcontext_vaapi.h>
#include <libavutil/opt.h>
#include <libavutil/avassert.h>
#include <libavutil/imgutils.h>
#include <va/va.h>

#include "gbm_buf.h"
#include "fbdev.h"
#include "drm_draw.h"
#include "vaapi_stuff.h"

#define SIZE 3840 * 2160 * 4

struct display {
    int drm_fd;
    struct fbdev *fb;
};

static AVBufferRef *hw_device_ctx = NULL;
static enum AVPixelFormat hw_pix_fmt;
static FILE *output_file = NULL;

static int hw_decoder_init(AVCodecContext *ctx, const enum AVHWDeviceType type)
{
    int err = 0;

    if ((err = av_hwdevice_ctx_create(&hw_device_ctx, type,
                                      NULL, NULL, 0)) < 0) {
        fprintf(stderr, "Failed to create specified HW device.\n");
        return err;
    }
    ctx->hw_device_ctx = av_buffer_ref(hw_device_ctx);

    return err;
}

static enum AVPixelFormat get_hw_format(AVCodecContext *ctx,
                                        const enum AVPixelFormat *pix_fmts)
{
    const enum AVPixelFormat *p;

    for (p = pix_fmts; *p != -1; p++) {
        if (*p == hw_pix_fmt)
            return *p;
    }

    fprintf(stderr, "Failed to get HW surface format.\n");
    return AV_PIX_FMT_NONE;
}

static int get_image_format(AVHWDeviceContext *hwdev,
                            enum AVPixelFormat pix_fmt,
                            VAImageFormat **image_format)
{
    VDeviceContext *ctx = hwdev->internal->priv;
    int i;

    for (i = 0; i < ctx->nb_formats; i++) {
        if (ctx->formats[i].pix_fmt == pix_fmt) {
            if (image_format)
                *image_format = &ctx->formats[i].image_format;
            return 0;
        }
    }
    return -1;
}

static int process_frame(AVCodecContext *avctx, AVFrame *src,
        AVCodecParameters *codecpar,
        struct buffer *buffer, struct display *d)
{
    AVHWFramesContext* hwfc = (AVHWFramesContext*)src->hw_frames_ctx->data;
    AVVAAPIDeviceContext *hwctx = hwfc->device_ctx->hwctx;
    VASurfaceID surface_id;
    VAImage image;
    VAImageFormat *image_format;
    VAStatus vas;
    int flags;
    int err, i;

    surface_id = (VASurfaceID)(uintptr_t)src->data[3];
    flags = AV_HWFRAME_MAP_OVERWRITE;
    image.image_id = VA_INVALID_ID;

    err = get_image_format(hwfc->device_ctx, hwfc->sw_format, &image_format);
    if (err < 0) {
        fprintf(stderr, "Failed to get imagef format, err=%d\n", err);
        return -1;
    }

    vas = vaSyncSurface(hwctx->display, surface_id);
    if (vas != VA_STATUS_SUCCESS) {
        fprintf(stderr, "Failed to sync va surface id %d\n", surface_id);
        return -1;
    }

    vas = vaCreateImage(hwctx->display, image_format,
                        hwfc->width, hwfc->height, &image);
    if (vas != VA_STATUS_SUCCESS) {
        fprintf(stderr, "Failed to create image, err %d\n", vas);
        return -1;
    }

    vas = vaGetImage(hwctx->display, surface_id, 0, 0,
                     hwfc->width, hwfc->height, image.image_id);
    if (vas != VA_STATUS_SUCCESS) {
        fprintf(stderr, "Failed to read image from surface %#x: %d (%s).\n",
                surface_id, vas, vaErrorStr(vas));
        return -1;
    }

    vas = vaMapBuffer(hwctx->display, image.buf, (void **)&buffer->mmap);
    if (vas != VA_STATUS_SUCCESS) {
        fprintf(stderr, "Failed to map image buffer\n");
        return -1;
    }

    /* Show the content received to display */
    fbdev_show(buffer->mmap, image.data_size, d->fb);
    return 0;
}

static int decode_write(AVCodecContext *avctx, AVPacket *packet,
        struct buffer *b, struct display *d,
        AVCodecParameters *codecpar)
{
    AVFrame *frame = NULL;
    int ret;

    ret = avcodec_send_packet(avctx, packet);
    if (ret < 0) {
        fprintf(stderr, "Error during decoding\n");
        return ret;
    }

    while (1) {
        frame = av_frame_alloc();
        if (!frame) {
            fprintf(stderr, "Error setting custom frame\n");
            goto fail;
        }

        ret = avcodec_receive_frame(avctx, frame);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
            av_frame_free(&frame);
            return 0;
        } else if (ret < 0) {
            fprintf(stderr, "Error while decoding\n");
            goto fail;
        }

        ret = process_frame(avctx, frame, codecpar, b, d);
        if (ret) {
            fprintf(stderr, "Failed to process\n");
            goto fail;
        }

fail:
        av_frame_free(&frame);
        if (ret < 0)
            return ret;
        return 0;
    }
}

int main(int argc, char *argv[])
{
    AVFormatContext *input_ctx = NULL;
    int video_stream, ret;
    AVStream *video = NULL;
    AVCodecContext *decoder_ctx = NULL;
    const AVCodec *decoder = NULL;
    AVPacket *packet = NULL;
    enum AVHWDeviceType type = AV_HWDEVICE_TYPE_VAAPI;
    struct buffer *b;
    struct display d;
    struct fbdev *fb;
    int drm_fd;
    int i;

    if (argc < 2) {
        fprintf(stderr, "Usage: %s <input file> <output file>\n", argv[0]);
        return -1;
    }

    packet = av_packet_alloc();
    if (!packet) {
        fprintf(stderr, "Failed to allocate AVPacket\n");
        return -1;
    }

    /* open the input file */
    if (avformat_open_input(&input_ctx, argv[1], NULL, NULL) != 0) {
        fprintf(stderr, "Cannot open input file '%s'\n", argv[1]);
        return -1;
    }

    if (avformat_find_stream_info(input_ctx, NULL) < 0) {
        fprintf(stderr, "Cannot find input stream information.\n");
        return -1;
    }

    /* find the video stream information */
    ret = av_find_best_stream(input_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, &decoder, 0);
    if (ret < 0) {
        fprintf(stderr, "Cannot find a video stream in the input file\n");
        return -1;
    }
    video_stream = ret;

    for (i = 0;; i++) {
        const AVCodecHWConfig *config = avcodec_get_hw_config(decoder, i);
        if (!config) {
            fprintf(stderr, "Decoder %s does not support device type %s.\n",
                    decoder->name, av_hwdevice_get_type_name(type));
            return -1;
        }
        if (config->methods & AV_CODEC_HW_CONFIG_METHOD_HW_DEVICE_CTX &&
            config->device_type == type) {
            hw_pix_fmt = config->pix_fmt;
            break;
        }
    }

    if (!(decoder_ctx = avcodec_alloc_context3(decoder)))
        return AVERROR(ENOMEM);

    video = input_ctx->streams[video_stream];
    if (avcodec_parameters_to_context(decoder_ctx, video->codecpar) < 0)
        return -1;

    decoder_ctx->get_format  = get_hw_format;

    if (hw_decoder_init(decoder_ctx, type) < 0)
        return -1;

    if ((ret = avcodec_open2(decoder_ctx, decoder, NULL)) < 0) {
        fprintf(stderr, "Failed to open codec for stream #%u\n", video_stream);
        return -1;
    }

    b = create_dma_buffer(decoder_ctx->width, decoder_ctx->height,
            decoder_ctx->pix_fmt);
    if (!b) {
        fprintf(stderr, "Failed to create dma buf\n");
        return -1;
    }

    /* Setup fbdev to display the frames */
    fb = fbdev_setup();
    if (!fb) {
        fprintf(stderr, "Failed to open FB\n");
    }
    d.fb = fb;

#if 1    
    /* Optinal setup of drm device if we want to flip buffers atomically */
    drm_fd = drm_init();
    if (drm_fd < 0) {
        fprintf(stderr, "Failed to open DRM\n");
    }
    d.drm_fd = drm_fd;
#endif

    /* actual decoding and dump the raw data */
    while (ret >= 0) {
        if ((ret = av_read_frame(input_ctx, packet)) < 0)
            break;

        if (video_stream == packet->stream_index)
            ret = decode_write(decoder_ctx, packet, b, &d, video->codecpar);

        av_packet_unref(packet);
    }

    /* flush the decoder */
    ret = decode_write(decoder_ctx, NULL, b, &d, video->codecpar);

    /* Cleanup */
    av_packet_free(&packet);
    avcodec_free_context(&decoder_ctx);
    avformat_close_input(&input_ctx);
    av_buffer_unref(&hw_device_ctx);
    destroy_dma_buffer(b);
    if (fb)
        fbdev_close(fb);
    return 0;
}
