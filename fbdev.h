struct fbdev {
	int fb_fd;
	int screensize;
	uint8_t *fbp;
};

void fbdev_close(struct fbdev *fb);

int fbdev_show(uint8_t *data, int size, struct fbdev *fb);

int fbdev_show_fix(uint8_t *data, int size, struct fbdev *fb);

struct fbdev* fbdev_setup(void);